#include <iostream>
#include <string>

using namespace std;

string seq_from_fasta(const string& filename);
uint64_t getMemorySelfMaxUsed ();
double koToBit(double ko);
string intToString(uint64_t num);
void progressionBar();
