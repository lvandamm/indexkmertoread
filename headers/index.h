#include <iostream>
#include <unordered_map>
#include <vector>
#include <string>

using namespace std;

struct occ_in_read {
            int total_count;
            int read_id;
        };

typedef unordered_map<string, vector<int>> umap;
typedef vector<occ_in_read> vect_occ_read;

class Index{

    private:
        string filename;
        int k;
        umap index;
        uint32_t nb_kmers;
        
    public:
        Index(string& filename, int k);

        void set_index(umap& index);
        umap get_index();

        void set_nb_kmers(uint32_t index);
        uint32_t get_nb_kmers();

        void index_fasta(const string& filename, int k);
        vect_occ_read query_sequence(const string& sequence);
        vector<int> query_kmer(const string& kmer);
        vect_occ_read query_fasta(const string& filename);
        void write_index(const string& output_file);
};