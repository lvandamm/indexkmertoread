CC = g++

CFLAGS= -O3 -march=native -std=c++11 -Wall

EXEC = index
OBJ = index.o main.o utils.o

all : $(EXEC) 

index : $(OBJ)
	$(CC) -o index $^ $(CFLAGS)

index.o: sources/index.cpp
	$(CC) -o $@ -c $< $(CFLAGS)

main.o: sources/main.cpp
	$(CC) -o $@ -c $< $(CFLAGS)

utils.o: sources/utils.cpp
	$(CC) -o $@ -c $< $(CFLAGS)

clean:
	rm -rf $(EXEC) $(OBJ)

rebuild: clean $(EXEC)