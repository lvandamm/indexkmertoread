#include <iostream>
#include <string>
#include <fstream>
#include <sys/resource.h>
#include "../headers/index.h"

using namespace std;

string seq_from_fasta(const string& filename){

    ifstream fichier(filename, ios::in);
    string seq;
    if(fichier){   
        string ligne;
        while(getline(fichier,ligne)){
            if (ligne[0] != '>'){
                seq = ligne;
            }
        }
        fichier.close();
    }
    
    else cerr << "Error opening the file." << endl;
    return seq;
}

uint64_t getMemorySelfMaxUsed (){
	uint64_t result = 0;
	struct rusage usage;
	if (getrusage(RUSAGE_SELF, &usage)==0){  
        result = usage.ru_maxrss;  
    }
	return result;
}

double koToBit(double ko){
  return ko * 8000;
}

string intToString(uint64_t num) {
  string s = std::to_string(num);

  int n = s.length() - 3;
  int end = (num >= 0) ? 0 : 1;
  while (n > end) {
    s.insert(n, ",");
    n -= 3;
  }
  return s;
}
