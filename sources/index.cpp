#include <iostream>
#include <unordered_map>
#include <vector>
#include <string>
#include <fstream>
#include "../headers/index.h"

using namespace std;
using std::find;

Index::Index(string& fastaFilename, int kmerLength){
    filename = fastaFilename;
    k = kmerLength;
}

void Index::set_index(umap& index){
    this->index = index;
}

umap Index::get_index(){
    return this->index;
}

void Index::set_nb_kmers(uint32_t nb_kmers){
    this-> nb_kmers = nb_kmers;
}

uint32_t Index::get_nb_kmers(){
    return this->nb_kmers;
}


// INDEXATION

void Index::index_fasta(const string& read_file, int k){

    /**
     * @brief Associate kmers to the reads to which they belong
     * @param read_file (string) the name of the file containing reads
     * @param k (int) the kmers length
     * @return (unordered_map) a hashmap (keys : kmers (string) ; values : vector<int> representing IDs for the reads)
     */

    umap hmap;
    uint32_t num_read = 0;
    uint32_t num_kmers = 0;
    ifstream fichier(read_file, ios::in);
    if(fichier)
        {   
            string ligne;
            while(!fichier.eof()){
                getline(fichier,ligne);
                if (ligne[0] != '>' && !ligne.empty()){ // ONLY GET SEQUENCES
                    for (long unsigned int i=0; i<=ligne.length()-k; i++){
                        num_kmers ++;
                        string kmer = ligne.substr(i, k);
                        hmap[kmer].push_back(num_read); // CHECK IF THE KMER IS ALREADY IN THE HASHMAP AND INSERT
                    }
                    num_read ++;
                }
            }
            fichier.close();
        }
    else{
        cerr << "Error opening the file." << endl;
    }
    set_nb_kmers(num_kmers);
    set_index(hmap);
}


// KMER SCALE

vector<int> Index::query_kmer(const string& kmer){

    /**
     * @brief Query a k-mer in an index structure.
     * @param kmer (string) the k-mer we are searching for.
     * @return (vector<int>) the list of reads in which the k-mer appears.
     */

    try{
        return index.at(kmer);
    }
    
    catch(const out_of_range& err) {
        cerr << "Out of Range error: " << err.what() << '\n';
        vector<int> v;
        return v;
    }
    
}


// READ SCALE

vect_occ_read Index::query_sequence(const string& sequence){

    /**
     * @brief Query a read in an index structure. The read will be first divided into k-mers.
     * @param read (string) the read we'll divide in k-mers and search for in the index.
     * @return (vector<int, vector<int>>) the list of reads in which the k-mers appears and their number of occurences.
     */

    vect_occ_read res;
    unordered_map<int, int> tmp_reads;
    for (long unsigned int i=0; i<=sequence.length()-this->k; i++){
        string kmer = sequence.substr(i, this->k);
        vector<int> reads_of_kmer = query_kmer(kmer);
        for (auto& idRead : reads_of_kmer){
            if(tmp_reads.find(idRead) != tmp_reads.end()){
                tmp_reads[idRead] ++;
            }
            else{
                tmp_reads[idRead] = 1;
            }
        }
    }

    for (auto const &pair: tmp_reads){
        occ_in_read structRead;
        structRead.read_id = pair.first;
        structRead.total_count = pair.second;
        res.push_back(structRead);
    }

    return res;
}


// FASTA SCALE

vect_occ_read Index::query_fasta(const string& filename){

    /**
     * @brief Search, for all the reads in the file, and for all their kmers, in which read they appear.
     * @param filename (string) the file containing the reads.
     * @return (vector<vector<occ_in_read>>) for each read, the list of reads in which the kmers appear.
     */
    
    vect_occ_read res;
    unordered_map<int, int> tmp_reads;

    ifstream fichier(filename, ios::in);

    if(fichier){   
        string ligne;
        while( !fichier.eof()){
            getline(fichier,ligne);
            if (ligne[0] != '>' && !ligne.empty()){
                vect_occ_read occ_read = query_sequence(ligne);
                for (auto& struct_read : occ_read){
                    int read_id = struct_read.read_id;
                    int total_count = struct_read.total_count;
                    if(tmp_reads.find(read_id) != tmp_reads.end()){
                        tmp_reads[read_id] += total_count;
                    }
                    else{
                        tmp_reads[read_id] = total_count;
                    }
                }
            }
        }
        for (auto const &pair: tmp_reads){
            occ_in_read structRead;
            structRead.read_id = pair.first;
            structRead.total_count = pair.second;
            res.push_back(structRead);
        }
    fichier.close();
    return res;
    }
    else{
        cerr << "Error opening the file." << endl;
        return res;
    }
}


// UTILS

void Index::write_index(const string& output_file){

    /**
     * @brief Query a k-mer in an index structure.
     * @param kmer (string) the k-mer we are searching for.
     * @return (vector<int>) the list of reads in which the k-mer appears.
     */

    ofstream outp(output_file);

    if (outp){
        for (auto const &pair: index) {
            outp << pair.first << ": ";
            for (int i : pair.second){
                outp << i << ","; 
            }
        outp << "\n";
        }
    }

    outp.close();
}