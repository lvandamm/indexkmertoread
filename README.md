## Compile

make

## Launch 

#### Arguments :

- Fasta file (reads) : path to the fasta file containing reads
- K-mer length

Used to create the index.

#### Options :

- Fasta file (containing 1 kmer to search it in the index)
- Fasta file (containing 1 sequence, to search and count kmers in the index)
- Fasta file (containing a list of sequences, to search and count all the kmers in the index)

<code>
./index [Fasta file (reads)] [Kmer length] -k [Fasta file (1 kmer)] -s [Fasta file (1 sequence)] -f [Fasta file (several sequences)]
</code>

Example :

<code>
./index input_files/phage_5kb_10percent.fa 15 -k input_files/kmer.fa -s input_files/sequence.fa
</code>