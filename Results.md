# Results

## Comparison between an index containing reads (keys : kmers; value : vector of integers), and an empty index (keys : kmers; value : boolean)

- Empty index : 2 seconds, 754 Mo, 0.16Ko/k-mer
- Full index : 4 seconds, 842 Mo, 0.17Ko/k-mer

It seems more important to compress the unordered_map than the vectors.

## Indexing with k-mers of size 15 
### Using phage_5kb_10percent.fa

|      |   Time |  Resource usage |  Resource usage (1 k-mer) |
|---    |:-:    |:-:    |:-:    |
|   Naive version   |   4 seconds   |   842 Mo |   0.17 Ko |
|   Robin Hood Hashtable <br/>& K_mer uint64_t   |   2 seconds  |   763 Mo |   0.16 Ko |
|   Delta encoding   | 2 seconds | 763 Mo | 0.16 Ko |


## Times for querying the index 
### Using phage_5kb_10percent.fa

|      |   Querying a k-mer |  Querying a sequence | Querying a set of sequences |
|---    |:-:    |:-:    |:-:    |
|   Naive version   |   1 seconds   |   0 seconds |   0 seconds |
|   Robin Hood Hashtable <br/>& K_mer uint64_t   | 0 seconds  | 0 seconds | 0 seconds |
|   Delta encoding   | 0 seconds | 0 seconds | 0 seconds |


